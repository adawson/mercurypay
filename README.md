RevoPos
=======

Mercury Pay Module
------------------

Credit Sale XML Request
-----------------------

<?xml version="1.0"?>
<TStream>
    <Transaction>
        <MerchantID>003503902913105</MerchantID>
        <LaneID>02</LaneID>
        <TranType>Credit</TranType>
        <TranCode>Sale</TranCode>
        <InvoiceNo>10</InvoiceNo>
        <RefNo>10</RefNo>
        <Memo>ExamplePOS V4.2</Memo>
        <Account>
            <EncryptedFormat>MagneSafe</EncryptedFormat>
            <AccountSource>Swiped</AccountSource>
            <EncryptedBlock>F40DDBA1F645CC8DB85A6459D45AFF8002C244A0F4402B479ABC9915EC9567C81BE99CE4483AF3D</EncryptedBlock>
            <EncryptedKey>9012090B01C4F200002B</EncryptedKey>
        </Account>
        <Amount>
            <Purchase>1.05</Purchase>
        </Amount>
    </Transaction>
</TStream>

Example Swiper Response
-----------------------

0~IPAD100KB|24~4353047B121C130A|1~21|2~5E97C48238415ACB9C82F4E8E71746127881B8A71FFBB3CC1DCE4B5B83562A871DCC5025C21ECE1BF037C5B069BEA18E|3~ACFBE99088544FDE8393AF9FF2817F5D2E28F99590F7C82C487096465107966E907AA64B45792E4A|4~|5~1F4E65350FD407CACFDC01C9964AC771C88AA7546CC2E553912DA608216A9519675C6FF38FDA2DAD94F0383062AB6A414F39C42E623568D0|6~%B4003000001006781^TEST/MPS^13050000000000000?|7~;4003000001006781=13050000000000000000?|8~|9~00000000|10~000001|11~9500570000008720001D|12~00002200|13~9A006300000009200004|14~8F8A60DFECF190AA|

The required EncryptedBlock and Key are fields 3~ and 11~;

Credit Sale XML Response
------------------------

<?xml version="1.0"?>
<RStream>
    <CmdResponse>
        <ResponseOrigin>Processor</ResponseOrigin>
        <DSIXReturnCode>000000</DSIXReturnCode>
        <CmdStatus>Approved</CmdStatus>
        <TextResponse>AP</TextResponse>
    </CmdResponse>
    <TranResponse>
        <MerchantID>003503902913105</MerchantID>
        <AcctNo>400300XXXXXX6781</AcctNo>
        <ExpDate>1215</ExpDate>
        <CardType>VISA</CardType>
        <TranCode>Sale</TranCode>
        <AuthCode>000017</AuthCode>
        <CaptureStatus>Captured</CaptureStatus>
        <RefNo>0016</RefNo>
        <InvoiceNo>10</InvoiceNo>
        <Memo>ExamplePOS V4.2</Memo>
        <Amount>
            <Purchase>1.05</Purchase>
            <Authorize>1.05</Authorize>
        </Amount>
        <AcqRefData>aEb001356567810105c0105d5e00</AcqRefData>
        <ProcessData>|00|410100201000</ProcessData>
    </TranResponse>
</RStream> 