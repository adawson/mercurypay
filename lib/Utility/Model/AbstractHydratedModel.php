<?php

namespace RevoPos\MercuryPay\Utility\Model;

/**
 * Class AbstractHydratedModel
 *
 * @package RevoPos\MercuryPay\Utility\Model
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
abstract class AbstractHydratedModel
{
    /**
     * Hydrate an object with a given assoc. array
     *
     * @param array $data
     * @param object $object
     * @param array $overrides
     * @return object
     */
    protected function hydrate(array $data, $object, array $overrides = array())
    {
        foreach (get_class_methods($object) as $method) {
            if (preg_match('/^set([a-z]+)$/i', $method, $matches)) {
                if (array_key_exists($matches[1], $overrides)) {
                    $object->{$method}($overrides[$matches[1]]);
                }
                else {
                    $object->{$method}(isset($data[$matches[1]]) ? $data[$matches[1]] : null);
                }
            }
        }
        return $object;
    }
}