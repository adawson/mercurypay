<?php

namespace RevoPos\MercuryPay\Processor;

/**
 * Class AbstractProcessor
 *
 * @package RevoPos\MercuryPay\Processor
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
abstract class AbstractProcessor extends \Pimple implements ProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);
        
        $this->initializeContainer();
    }

    /**
     * Initialise the container
     *
     * @return void
     */
    private function initializeContainer()
    {
        $services = require __DIR__ . '/../Resources/config/services.php';
        
        foreach ($services as $id => $closure) {
            $this[$id] = $closure;
        }
    }

    /**
     * Get service from container
     *
     * @param string $id
     * @return mixed
     */
    protected function getService($id)
    {
        return $this[$id];
    }

    /**
     * Convert an int or float into zero padded currency format, e.g.
     *
     * 3.27 -> 3.27
     * 4.2  -> 4.20
     * 7    -> 7.00
     *
     * @param int|float $amount
     * @return string
     */
    protected function transformFloatToCurrencyFormat($amount)
    {
        return number_format($amount, 2, '.', '');
    }
}