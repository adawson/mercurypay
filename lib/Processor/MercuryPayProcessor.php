<?php

namespace RevoPos\MercuryPay\Processor;

use RevoPos\MercuryPay\Enumeration\CmdResponseStatus;
use RevoPos\MercuryPay\Enumeration\Frequency;
use RevoPos\MercuryPay\Enumeration\PartialAuth;
use RevoPos\MercuryPay\Enumeration\RecordNo;
use RevoPos\MercuryPay\Enumeration\TranCode;
use RevoPos\MercuryPay\Enumeration\TranType;
use RevoPos\MercuryPay\Logger\TransactionLogger;
use RevoPos\MercuryPay\Model\Request\Amount;
use RevoPos\MercuryPay\Model\Request\MagneSafeSwipedAccount;
use RevoPos\MercuryPay\Model\Request\Transaction;
use RevoPos\MercuryPay\Model\Request\TStream;
use RevoPos\MercuryPay\Model\Response\PaymentResponse;
use RevoPos\MercuryPay\Model\Response\RStream;

/**
 * Class MercuryPayProcessor
 *
 * @package RevoPos\MercuryPay\Processor
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class MercuryPayProcessor extends AbstractProcessor
{
    /**
     * @var string
     */
    private $serviceWsdlUrl;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \SoapClient
     */
    private $soapClient;
    
    /**
     * @var TransactionLogger|null
     */
    private $txnLogger;

    /**
     * Constructor
     *
     * @param string $serviceWsdlUrl
     * @param string $merchantId
     * @param string $password
     * @param TransactionLogger|null $txnLogger
     */
    public function __construct($serviceWsdlUrl, $merchantId, $password, TransactionLogger $txnLogger = null)
    {
        $this->serviceWsdlUrl = $serviceWsdlUrl;
        $this->merchantId = $merchantId;
        $this->password = $password;
        $this->txnLogger = $txnLogger ?: new TransactionLogger();
        
        parent::__construct();
    }

    /**
     * Process a swiped payment
     *
     * <code>
     * $response = $processor->processSwipedPayment(
     *     '00000123', // Invoice Number
     *     'Example Memo Text',
     *     23.98, // Amount to be charged
     *     '0~IPAD100KB|24~4353047B121C130A|1~21|2~5E97C48238415ACB9C82F4E8E71746127881B8A71FFBB3CC3562A871...', // Swipe device output
     * );
     * </code>
     *
     * @param string $invoiceNumber
     * @param string $memo
     * @param float $amount
     * @param string $swipeDeviceOutput
     * @return PaymentResponse
     */
    public function processPreAuthSwipedPayment($invoiceNumber, $memo, $amount, $swipeDeviceOutput)
    {
        $account = new MagneSafeSwipedAccount();
        $account
            ->fromSwipeDevice($swipeDeviceOutput)
        ;

        $amountObj = new Amount();
        $amountObj
            ->setPurchase($this->transformFloatToCurrencyFormat($amount))
            ->setAuthorize($this->transformFloatToCurrencyFormat($amount))
        ;

        $transaction = new Transaction();
        $transaction
            ->setMerchantID($this->merchantId)
            ->setTranType(TranType::CREDIT)
            ->setTranCode(TranCode::PRE_AUTH)
            ->setRecordNo(RecordNo::RECORD_NUMBER_REQUESTED)
            ->setFrequency(Frequency::ONE_TIME)
            ->setPartialAuth(PartialAuth::ALLOW)
            ->setInvoiceNo($invoiceNumber)
            ->setMemo($memo)
            ->setAmount($amountObj)
            ->setAccount($account)
        ;

        $tStream = new TStream();
        $tStream
            ->setTransaction($transaction)
        ;

        $rStream = $this->executePaymentCommand($tStream, $rawResponse);
        
        $response = $this->parseResponse($rStream);

        $response->setRawResponse($rawResponse);
        
        return $response;
    }

    /**
     * Capture payment and adjust for gratuity
     *
     * @param string $authCode
     * @param string $invoiceNumber
     * @param string $memo
     * @param string $acqRefData
     * @param float $amount
     * @param float $gratuityAmount
     * @param string $swipeDeviceOutput
     * @return PaymentResponse
     */
    public function capturePaymentWithGratuitySwipedPayment($authCode, $invoiceNumber, $memo, $acqRefData, $amount,
                                                            $gratuityAmount, $swipeDeviceOutput)
    {
        $account = new MagneSafeSwipedAccount();
        $account
            ->fromSwipeDevice($swipeDeviceOutput)
        ;

        $amountObj = new Amount();
        $amountObj
            ->setPurchase($this->transformFloatToCurrencyFormat($amount))
            ->setAuthorize($this->transformFloatToCurrencyFormat($amount))
            ->setGratuity($this->transformFloatToCurrencyFormat($gratuityAmount))
        ;

        $transaction = new Transaction();
        $transaction
            ->setMerchantID($this->merchantId)
            ->setTranType(TranType::CREDIT)
            ->setTranCode(TranCode::PRE_AUTH_CAPTURE)
            ->setRecordNo(RecordNo::RECORD_NUMBER_REQUESTED)
            ->setFrequency(Frequency::ONE_TIME)
            ->setPartialAuth(PartialAuth::ALLOW)
            ->setInvoiceNo($invoiceNumber)
            ->setMemo($memo)
            ->setAmount($amountObj)
            ->setAccount($account)
            ->setAuthCode($authCode)
            ->setAcqRefData($acqRefData)
        ;

        $tStream = new TStream();
        $tStream
            ->setTransaction($transaction)
        ;

        $rStream = $this->executePaymentCommand($tStream, $rawResponse);

        $response = $this->parseResponse($rStream);

        $response->setRawResponse($rawResponse);

        return $response;
    }

    /**
     * Process void sale of swiped payment
     *
     * @param string $authCode
     * @param string $invoiceNumber
     * @param string $referenceNumber
     * @param string $memo
     * @param float $amount
     * @param float $gratuityAmount
     * @param string $swipeDeviceOutput
     * @return PaymentResponse
     */
    public function processVoidSaleSwipedPayment($authCode, $invoiceNumber, $referenceNumber, $memo, $amount, $gratuityAmount,
                                                 $swipeDeviceOutput)
    {
        $account = new MagneSafeSwipedAccount();
        $account
            ->fromSwipeDevice($swipeDeviceOutput)
        ;

        $amountObj = new Amount();
        $amountObj
            ->setPurchase($this->transformFloatToCurrencyFormat($amount))
            ->setGratuity($this->transformFloatToCurrencyFormat($gratuityAmount))
        ;
        
        $transaction = new Transaction();
        $transaction
            ->setMerchantID($this->merchantId)
            ->setTranType(TranType::CREDIT)
            ->setTranCode(TranCode::VOID_SALE)
            ->setRecordNo(RecordNo::RECORD_NUMBER_REQUESTED)
            ->setFrequency(Frequency::ONE_TIME)
            ->setAuthCode($authCode)
            ->setInvoiceNo($invoiceNumber)
            ->setRefNo($referenceNumber)
            ->setMemo($memo)
            ->setAccount($account)
            ->setAmount($amountObj)
        ;
        
        $tStream = new TStream();
        $tStream
            ->setTransaction($transaction)
        ;

        $rStream = $this->executePaymentCommand($tStream, $rawResponse);

        $response = $this->parseResponse($rStream);

        $response->setRawResponse($rawResponse);

        return $response;
    }

    /**
     * Parse the response from the payment processor
     *
     * @param RStream $rStream
     * @return PaymentResponse
     */
    private function parseResponse(RStream $rStream)
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setRStream($rStream);

        switch ($rStream->getCmdResponse()->getTextResponse()) {

            case CmdResponseStatus::AP:
            case CmdResponseStatus::APPROVED:
            case CmdResponseStatus::AP_NEW_INFO:
            case CmdResponseStatus::APPROVED_STANDIN:
            case CmdResponseStatus::AP_DUPE:
            case CmdResponseStatus::AP_STAR:
                $paymentResponse->setIsSuccess(true);
                $paymentResponse->setMessage('Approved');
                break;

            case CmdResponseStatus::PARTIAL_AP:
                $paymentResponse->setIsSuccess(true);
                $paymentResponse->setMessage('Partially Approved');
                break;

            case CmdResponseStatus::REVERSED:
                $paymentResponse->setIsSuccess(true);
                $paymentResponse->setMessage('Reversed');
                break;

            case CmdResponseStatus::AP_NOT_CAPTURED:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Approved but Not Captured (possible MercuryPay network outage)');
                break;

            case CmdResponseStatus::DECLINE:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Card Declined');
                break;

            case CmdResponseStatus::PIC_UP:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Card Declined - Alert: Please retain card for reporting');
                break;
            
            case CmdResponseStatus::DECLINED_CV2_FAIL:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Card Declined - CV2 mismatch');
                break;

            case CmdResponseStatus::INVALID_EXP_DATE:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Card Declined - Invalid expiry date');
                break;

            case CmdResponseStatus::INVALID_PIN:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Card Declined - Pin invalid');
                break;

            case CmdResponseStatus::UNAUTH_USER:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Unauthorised user');
                break;

            case CmdResponseStatus::NO_TRAN_FOUND:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Failed reversal attempt');
                break;

            case CmdResponseStatus::INV_ITEM_NUMBER:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Invalid item number');
                break;

            case CmdResponseStatus::CALL_CENTER:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Call for voice authorization');
                break;

            case CmdResponseStatus::CALL_AE:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage('Call AMEX for voice authorization');
                break;

            default:
                $paymentResponse->setIsSuccess(false);
                $paymentResponse->setMessage($rStream->getCmdResponse()->getTextResponse());
                break;
        }

        return $paymentResponse;
    }

    /**
     * Execute payment command
     *
     * @param TStream $paymentRequest
     * @param string $rawResponse
     * @return RStream
     */
    private function executePaymentCommand(TStream $paymentRequest, &$rawResponse)
    {
        $requestXml = $this->getRequestSerializer()->serialize($paymentRequest, 'xml');

        $this->txnLogger->setRequest($requestXml);

        $this->txnLogger->setPassword($this->password);
        $this->txnLogger->setUrl($this->serviceWsdlUrl);
        $this->txnLogger->setMerchantId($this->merchantId);
        
        $request = array(
            'tran' => $requestXml,
            'pw' => $this->password,
        );

        $response = $this->getSoapClient()->CreditTransaction($request);

        $rawResponse = $response->CreditTransactionResult;

        $this->txnLogger->setResponse($rawResponse);
        
        return $this->getResponseSerializer()->deserialize(
            $rawResponse,
            'RevoPos\MercuryPay\Model\Response\RStream',
            'xml'
        );
    }

    /**
     * Get request serializer
     *
     * @return \Symfony\Component\Serializer\Serializer
     */
    private function getRequestSerializer()
    {
        return $this->getService('request_serializer');
    }

    /**
     * Get response serializer
     *
     * @return \Symfony\Component\Serializer\Serializer
     */
    private function getResponseSerializer()
    {
        return $this->getService('response_serializer');
    }

    /**
     * Get initialized SOAP client
     *
     * @return \SoapClient
     */
    private function getSoapClient()
    {
        if ( ! $this->soapClient) {
            $this->soapClient = new \SoapClient($this->serviceWsdlUrl);
        }

        return $this->soapClient;
    }

    /**
     * Get txnLogger
     *
     * @return null|TransactionLogger
     */
    public function getTxnLogger()
    {
        return $this->txnLogger;
    }
}
