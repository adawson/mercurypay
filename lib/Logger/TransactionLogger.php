<?php

namespace RevoPos\MercuryPay\Logger;

/**
 * Class TransactionLogger
 *
 * @package RevoPos\MercuryPay\Logger
 */
class TransactionLogger
{
    /**
     * @var string
     */
    private $request;
    
    /**
     * @var string
     */
    private $response;
    
    /**
     * @var string
     */
    private $url;
    
    /**
     * @var string
     */
    private $merchantId;
    
    /**
     * @var string
     */
    private $password;

    /**
     * Get request
     *
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return TransactionLogger
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Get response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return TransactionLogger
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return TransactionLogger
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get merchantId
     *
     * @return string
     */
    public function getMerchantId()
    {
        return $this->merchantId;
    }

    /**
     * Set merchantId
     *
     * @param string $merchantId
     * @return TransactionLogger
     */
    public function setMerchantId($merchantId)
    {
        $this->merchantId = $merchantId;
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return TransactionLogger
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}