<?php

/**
 * @var array
 *
 * Service container configuration
 */
return array(

    /**
     * Request root node name
     */
    'request_serializer_encoder.root_node_name' => 'TStream',

    /**
     * Response root node name
     */
    'response_serializer_encoder.root_node_name' => 'RStream',

    /**
     * Request Normalizer for Serializer
     */
    'serializer_normalizer' => function () {
            return new RevoPos\MercuryPay\Serializer\UcFirstGetSetMethodNormalizer();
        },

    /**
     * Encoder for Request Serializer
     */
    'request_serializer_encoder' => function ($c) {
            return new Symfony\Component\Serializer\Encoder\XmlEncoder($c['request_serializer_encoder.root_node_name']);
        },

    /**
     * Request Serializer
     */
    'request_serializer' => function ($c) {
            return new Symfony\Component\Serializer\Serializer(
                array($c['serializer_normalizer']),
                array($c['request_serializer_encoder'])
            );
        },

    /**
     * Encoder for Response Serializer
     */
    'response_serializer_encoder' => function ($c) {
            return new Symfony\Component\Serializer\Encoder\XmlEncoder($c['response_serializer_encoder.root_node_name']);
        },

    /**
     * Response Serializer
     */
    'response_serializer' => function ($c) {
            return new Symfony\Component\Serializer\Serializer(
                array($c['serializer_normalizer']),
                array($c['response_serializer_encoder'])
            );
        },
);