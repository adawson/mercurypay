<?php

namespace RevoPos\MercuryPay\Model\Request;

/**
 * Class TStream
 *
 * @package RevoPos\MercuryPay\Model\Request
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class TStream
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * Get transaction
     *
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Set transaction
     *
     * @param Transaction $transaction
     * @return $this
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }
}