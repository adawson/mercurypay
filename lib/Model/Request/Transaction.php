<?php

namespace RevoPos\MercuryPay\Model\Request;

/**
 * Class Transaction
 *
 * @package RevoPos\MercuryPay\Model\Request
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class Transaction
{
    /**
     * @var string
     */
    private $merchantID;

    /**
     * @var string
     */
    private $laneID;

    /**
     * @var string
     */
    private $tranType;

    /**
     * @var string
     */
    private $tranCode;

    /**
     * @var string
     */
    private $invoiceNo;

    /**
     * @var string
     */
    private $refNo;

    /**
     * @var string
     */
    private $recordNo;

    /**
     * @var string
     */
    private $frequency;

    /**
     * @var string
     */
    private $memo;

    /**
     * @var string
     */
    private $partialAuth;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var Amount
     */
    private $amount;

    /**
     * @var string
     */
    private $authCode;

    /**
     * @var string
     */
    private $acqRefData;

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set account
     *
     * @param Account $account
     * @return $this
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * Get acqRefData
     *
     * @return string
     */
    public function getAcqRefData()
    {
        return $this->acqRefData;
    }

    /**
     * Set acqRefData
     *
     * @param string $acqRefData
     * @return Transaction
     */
    public function setAcqRefData($acqRefData)
    {
        $this->acqRefData = $acqRefData;
        return $this;
    }

    /**
     * Get amount
     *
     * @return Amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amount
     *
     * @param Amount $amount
     * @return $this
     */
    public function setAmount(Amount $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get invoiceNo
     *
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set invoiceNo
     *
     * @param string $invoiceNo
     * @return $this
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;
        return $this;
    }

    /**
     * Get laneID
     *
     * @return string
     */
    public function getLaneID()
    {
        return $this->laneID;
    }

    /**
     * Set laneID
     *
     * @param string $laneID
     * @return $this
     */
    public function setLaneID($laneID)
    {
        $this->laneID = $laneID;
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set memo
     *
     * @param string $memo
     * @return $this
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
        return $this;
    }

    /**
     * Get merchantID
     *
     * @return string
     */
    public function getMerchantID()
    {
        return $this->merchantID;
    }

    /**
     * Set merchantID
     *
     * @param string $merchantID
     * @return $this
     */
    public function setMerchantID($merchantID)
    {
        $this->merchantID = $merchantID;
        return $this;
    }

    /**
     * Get frequency
     *
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set frequency
     *
     * @param string $frequency
     * @return $this
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
        return $this;
    }

    /**
     * Get partialAuth
     *
     * @return string
     */
    public function getPartialAuth()
    {
        return $this->partialAuth;
    }

    /**
     * Set partialAuth
     *
     * @param string $partialAuth
     * @return Transaction
     */
    public function setPartialAuth($partialAuth)
    {
        $this->partialAuth = $partialAuth;
        return $this;
    }

    /**
     * Get recordNo
     *
     * @return string
     */
    public function getRecordNo()
    {
        return $this->recordNo;
    }

    /**
     * Set recordNo
     *
     * @param string $recordNo
     * @return $this
     */
    public function setRecordNo($recordNo)
    {
        $this->recordNo = $recordNo;
        return $this;
    }

    /**
     * Get refNo
     *
     * @return string
     */
    public function getRefNo()
    {
        return $this->refNo;
    }

    /**
     * Set refNo
     *
     * @param string $refNo
     * @return $this
     */
    public function setRefNo($refNo)
    {
        $this->refNo = $refNo;
        return $this;
    }

    /**
     * Get tranCode
     *
     * @return string
     */
    public function getTranCode()
    {
        return $this->tranCode;
    }

    /**
     * Set tranCode
     *
     * @param string $tranCode
     * @return $this
     */
    public function setTranCode($tranCode)
    {
        $this->tranCode = $tranCode;
        return $this;
    }

    /**
     * Get tranType
     *
     * @return string
     */
    public function getTranType()
    {
        return $this->tranType;
    }

    /**
     * Set tranType
     *
     * @param string $tranType
     * @return $this
     */
    public function setTranType($tranType)
    {
        $this->tranType = $tranType;
        return $this;
    }

    /**
     * Get authCode
     *
     * @return string
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set authCode
     *
     * @param string $authCode
     * @return Transaction
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;
        return $this;
    }
}
