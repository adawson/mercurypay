<?php

namespace RevoPos\MercuryPay\Model\Request;

/**
 * Class MagneSafeSwipedAccount
 *
 * @package RevoPos\MercuryPay\Model\Request
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class MagneSafeSwipedAccount extends Account
{
    /**
     * @var string
     */
    private $encryptedFormat = 'MagneSafe';

    /**
     * @var string
     */
    private $accountSource = 'Swiped';

    /**
     * @var string
     */
    private $encryptedBlock;

    /**
     * @var string
     */
    private $encryptedKey;

    /**
     * Get accountSource
     *
     * @return string
     */
    public function getAccountSource()
    {
        return $this->accountSource;
    }

    /**
     * Set accountSource
     *
     * @param string $accountSource
     * @return $this
     */
    public function setAccountSource($accountSource)
    {
        $this->accountSource = $accountSource;
        return $this;
    }

    /**
     * Get encryptedBlock
     *
     * @return string
     */
    public function getEncryptedBlock()
    {
        return $this->encryptedBlock;
    }

    /**
     * Set encryptedBlock
     *
     * @param string $encryptedBlock
     * @return $this
     */
    public function setEncryptedBlock($encryptedBlock)
    {
        $this->encryptedBlock = $encryptedBlock;
        return $this;
    }

    /**
     * Get encryptedFormat
     *
     * @return string
     */
    public function getEncryptedFormat()
    {
        return $this->encryptedFormat;
    }

    /**
     * Set encryptedFormat
     *
     * @param string $encryptedFormat
     * @return $this
     */
    public function setEncryptedFormat($encryptedFormat)
    {
        $this->encryptedFormat = $encryptedFormat;
        return $this;
    }

    /**
     * Get encryptedKey
     *
     * @return string
     */
    public function getEncryptedKey()
    {
        return $this->encryptedKey;
    }

    /**
     * Set encryptedKey
     *
     * @param string $encryptedKey
     * @return $this
     */
    public function setEncryptedKey($encryptedKey)
    {
        $this->encryptedKey = $encryptedKey;
        return $this;
    }

    /**
     * Hydrate the encrypted block and key from swipe device output
     *
     * @param string $swipeDeviceOutput
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function fromSwipeDevice($swipeDeviceOutput)
    {
        $fragments = explode('|', $swipeDeviceOutput);

        if ( ! count($fragments)) {
            throw new \InvalidArgumentException('Swipe device fragments not found');
        }

        $encryptedBlock = null;
        $encryptedKey = null;

        if (isset($fragments[3])) {
            $encryptedBlock = $fragments[3];
        }

        if (isset($fragments[9])) {
            $encryptedKey = $fragments[9];
        }

        if ((null === $encryptedBlock) || (null === $encryptedKey)) {
            throw new \InvalidArgumentException('Swipe device has not provided encrypted block or key');
        }

        $this
            ->setEncryptedBlock($encryptedBlock)
            ->setEncryptedKey($encryptedKey)
        ;

        return $this;
    }

//    /**
//     * Hydrate the encrypted block and key from swipe device output
//     *
//     * NOTE: OLD fragmentation algorithm
//     *
//     * @param string $swipeDeviceOutput
//     * @return $this
//     * @throws \InvalidArgumentException
//     */
//    public function fromSwipeDevice($swipeDeviceOutput)
//    {
//        $fragments = explode('|', $swipeDeviceOutput);
//
//        if ( ! count($fragments)) {
//            throw new \InvalidArgumentException('Swipe device fragments not found');
//        }
//
//        $encryptedBlock = null;
//        $encryptedKey = null;
//
//        foreach ($fragments as $fragment) {
//            if (preg_match('/^([0-9]+)~(.*)$/', $fragment, $matches)) {
//                if (3 == $matches[1]) {
//                    $encryptedBlock = $matches[2];
//                }
//                elseif (11 == $matches[1]) {
//                    $encryptedKey = $matches[2];
//                }
//            }
//        }
//
//        if ((null === $encryptedBlock) || (null === $encryptedKey)) {
//            throw new \InvalidArgumentException('Swipe device has not provided encrypted block or key');
//        }
//
//        $this
//            ->setEncryptedBlock($encryptedBlock)
//            ->setEncryptedKey($encryptedKey)
//        ;
//
//        return $this;
//    }
}
