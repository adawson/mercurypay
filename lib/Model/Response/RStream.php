<?php

namespace RevoPos\MercuryPay\Model\Response;

use RevoPos\MercuryPay\Utility\Model\AbstractHydratedModel;

/**
 * Class RStream
 *
 * @package RevoPos\MercuryPay\Model\Response
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class RStream extends AbstractHydratedModel
{
    /**
     * @var CmdResponse
     */
    private $cmdResponse;

    /**
     * @var TranResponse
     */
    private $tranResponse;

    /**
     * Get cmdResponse
     *
     * @return CmdResponse
     */
    public function getCmdResponse()
    {
        return $this->cmdResponse;
    }

    /**
     * Set cmdResponse
     *
     * @param array|CmdResponse $cmdResponse
     * @return $this
     */
    public function setCmdResponse($cmdResponse)
    {
        $this->cmdResponse = $cmdResponse;

        if (is_array($cmdResponse)) {
            $this->cmdResponse = $this->hydrate($cmdResponse, new CmdResponse());
        }

        return $this;
    }

    /**
     * Get tranResponse
     *
     * @return TranResponse
     */
    public function getTranResponse()
    {
        return $this->tranResponse;
    }

    /**
     * Set tranResponse
     *
     * @param array|TranResponse $tranResponse
     * @return $this
     */
    public function setTranResponse($tranResponse)
    {
        if (is_array($tranResponse)) {

            $amount = null;

            if (isset($tranResponse['Amount'])) {
                $amount = $this->hydrate($tranResponse['Amount'], new Amount());
            }

            $this->tranResponse = $this->hydrate($tranResponse, new TranResponse(), array(
                'Amount' => $amount,
            ));
        }
        else {
            $this->tranResponse = $tranResponse;
        }

        return $this;
    }
}