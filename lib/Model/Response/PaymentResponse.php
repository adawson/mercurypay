<?php

namespace RevoPos\MercuryPay\Model\Response;

/**
 * Class PaymentResponse
 *
 * @package RevoPos\MercuryPay\Model\Response
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class PaymentResponse
{
    /**
     * @var RStream
     */
    private $rStream;

    /**
     * @var bool
     */
    private $isSuccess = false;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $rawResponse;

    /**
     * Get rStream
     *
     * @return RStream
     */
    public function getRStream()
    {
        return $this->rStream;
    }

    /**
     * Set rStream
     *
     * @param RStream $rStream
     * @return PaymentResponse
     */
    public function setRStream($rStream)
    {
        $this->rStream = $rStream;
        return $this;
    }

    /**
     * Get isSuccess
     *
     * @return boolean
     */
    public function getIsSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * Set isSuccess
     *
     * @param boolean $isSuccess
     * @return PaymentResponse
     */
    public function setIsSuccess($isSuccess)
    {
        $this->isSuccess = $isSuccess;
        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return PaymentResponse
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get rawResponse
     *
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * Set rawResponse
     *
     * @param string $rawResponse
     * @return PaymentResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
        return $this;
    }
}