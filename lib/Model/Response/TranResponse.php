<?php

namespace RevoPos\MercuryPay\Model\Response;

/**
 * Class TranResponse
 *
 * @package RevoPos\MercuryPay\Model\Response
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class TranResponse
{
    /**
     * @var string
     */
    private $merchantID;

    /**
     * @var string
     */
    private $acctNo;

    /**
     * @var string
     */
    private $authCode;

    /**
     * @var string
     */
    private $expDate;

    /**
     * @var string
     */
    private $cardType;

    /**
     * @var string
     */
    private $tranCode;

    /**
     * @var string
     */
    private $refNo;

    /**
     * @var string
     */
    private $invoiceNo;

    /**
     * @var string
     */
    private $memo;

    /**
     * @var Amount
     */
    private $amount;

    /**
     * @var string
     */
    private $recordNo;

    /**
     * @var string
     */
    private $processData;

    /**
     * @var string
     */
    private $captureStatus;

    /**
     * @var string
     */
    private $acqRefData;

    /**
     * Get acctNo
     *
     * @return string
     */
    public function getAcctNo()
    {
        return $this->acctNo;
    }

    /**
     * Set acctNo
     *
     * @param string $acctNo
     * @return $this
     */
    public function setAcctNo($acctNo)
    {
        $this->acctNo = $acctNo;
        return $this;
    }

    /**
     * Get acqRefData
     *
     * @return string
     */
    public function getAcqRefData()
    {
        return $this->acqRefData;
    }

    /**
     * Set acqRefData
     *
     * @param string $acqRefData
     * @return TranResponse
     */
    public function setAcqRefData($acqRefData)
    {
        $this->acqRefData = $acqRefData;
        return $this;
    }
    
    /**
     * Get captureStatus
     *
     * @return string
     */
    public function getCaptureStatus()
    {
        return $this->captureStatus;
    }

    /**
     * Set captureStatus
     *
     * @param string $captureStatus
     * @return TranResponse
     */
    public function setCaptureStatus($captureStatus)
    {
        $this->captureStatus = $captureStatus;
        return $this;
    }

    /**
     * Get authCode
     *
     * @return string
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set authCode
     *
     * @param string $authCode
     * @return TranResponse
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;
        return $this;
    }

    /**
     * Get amount
     *
     * @return Amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amount
     *
     * @param Amount $amount
     * @return $this
     */
    public function setAmount(Amount $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set cardType
     *
     * @param string $cardType
     * @return $this
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
        return $this;
    }

    /**
     * Get expDate
     *
     * @return string
     */
    public function getExpDate()
    {
        return $this->expDate;
    }

    /**
     * Set expDate
     *
     * @param string $expDate
     * @return $this
     */
    public function setExpDate($expDate)
    {
        $this->expDate = $expDate;
        return $this;
    }

    /**
     * Get invoiceNo
     *
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set invoiceNo
     *
     * @param string $invoiceNo
     * @return $this
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set memo
     *
     * @param string $memo
     * @return $this
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
        return $this;
    }

    /**
     * Get merchantID
     *
     * @return string
     */
    public function getMerchantID()
    {
        return $this->merchantID;
    }

    /**
     * Set merchantID
     *
     * @param string $merchantID
     * @return $this
     */
    public function setMerchantID($merchantID)
    {
        $this->merchantID = $merchantID;
        return $this;
    }

    /**
     * Get processData
     *
     * @return string
     */
    public function getProcessData()
    {
        return $this->processData;
    }

    /**
     * Set processData
     *
     * @param string $processData
     * @return $this
     */
    public function setProcessData($processData)
    {
        $this->processData = $processData;
        return $this;
    }

    /**
     * Get recordNo
     *
     * @return string
     */
    public function getRecordNo()
    {
        return $this->recordNo;
    }

    /**
     * Set recordNo
     *
     * @param string $recordNo
     * @return $this
     */
    public function setRecordNo($recordNo)
    {
        $this->recordNo = $recordNo;
        return $this;
    }

    /**
     * Get refNo
     *
     * @return string
     */
    public function getRefNo()
    {
        return $this->refNo;
    }

    /**
     * Set refNo
     *
     * @param string $refNo
     * @return $this
     */
    public function setRefNo($refNo)
    {
        $this->refNo = $refNo;
        return $this;
    }

    /**
     * Get tranCode
     *
     * @return string
     */
    public function getTranCode()
    {
        return $this->tranCode;
    }

    /**
     * Set tranCode
     *
     * @param string $tranCode
     * @return $this
     */
    public function setTranCode($tranCode)
    {
        $this->tranCode = $tranCode;
        return $this;
    }
}