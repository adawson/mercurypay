<?php

namespace RevoPos\MercuryPay\Model\Response;

/**
 * Class CmdResponse
 *
 * @package RevoPos\MercuryPay\Model\Response
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class CmdResponse
{
    /**
     * @var string
     */
    private $responseOrigin;

    /**
     * @var string
     */
    private $dSIXReturnCode;

    /**
     * @var string
     */
    private $cmdStatus;

    /**
     * @var string
     */
    private $textResponse;

    /**
     * @var string
     */
    private $userTraceData;

    /**
     * Get cmdStatus
     *
     * @return string
     */
    public function getCmdStatus()
    {
        return $this->cmdStatus;
    }

    /**
     * Set cmdStatus
     *
     * @param string $cmdStatus
     * @return $this
     */
    public function setCmdStatus($cmdStatus)
    {
        $this->cmdStatus = $cmdStatus;
        return $this;
    }

    /**
     * Get dSIXReturnCode
     *
     * @return string
     */
    public function getDSIXReturnCode()
    {
        return $this->dSIXReturnCode;
    }

    /**
     * Set dSIXReturnCode
     *
     * @param string $dSIXReturnCode
     * @return $this
     */
    public function setDSIXReturnCode($dSIXReturnCode)
    {
        $this->dSIXReturnCode = $dSIXReturnCode;
        return $this;
    }

    /**
     * Get responseOrigin
     *
     * @return string
     */
    public function getResponseOrigin()
    {
        return $this->responseOrigin;
    }

    /**
     * Set responseOrigin
     *
     * @param string $responseOrigin
     * @return $this
     */
    public function setResponseOrigin($responseOrigin)
    {
        $this->responseOrigin = $responseOrigin;
        return $this;
    }

    /**
     * Get textResponse
     *
     * @return string
     */
    public function getTextResponse()
    {
        return $this->textResponse;
    }

    /**
     * Set textResponse
     *
     * @param string $textResponse
     * @return $this
     */
    public function setTextResponse($textResponse)
    {
        $this->textResponse = $textResponse;
        return $this;
    }

    /**
     * Get userTraceData
     *
     * @return string
     */
    public function getUserTraceData()
    {
        return $this->userTraceData;
    }

    /**
     * Set userTraceData
     *
     * @param string $userTraceData
     * @return $this
     */
    public function setUserTraceData($userTraceData)
    {
        $this->userTraceData = $userTraceData;
        return $this;
    }
}
