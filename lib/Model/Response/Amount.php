<?php

namespace RevoPos\MercuryPay\Model\Response;

/**
 * Class Amount
 *
 * @package RevoPos\MercuryPay\Model\Response
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class Amount
{
    /**
     * @var float
     */
    private $purchase;

    /**
     * @var float
     */
    private $authorize;

    /**
     * @var float
     */
    private $gratuity;

    /**
     * Get authorize
     *
     * @return float
     */
    public function getAuthorize()
    {
        return $this->authorize;
    }

    /**
     * Set authorize
     *
     * @param float $authorize
     * @return $this
     */
    public function setAuthorize($authorize)
    {
        $this->authorize = $authorize;
        return $this;
    }

    /**
     * Get purchase
     *
     * @return float
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * Set purchase
     *
     * @param float $purchase
     * @return $this
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
        return $this;
    }

    /**
     * Get gratuity
     *
     * @return float
     */
    public function getGratuity()
    {
        return $this->gratuity;
    }

    /**
     * Set gratuity
     *
     * @param float $gratuity
     * @return Amount
     */
    public function setGratuity($gratuity)
    {
        $this->gratuity = $gratuity;
        return $this;
    }
}