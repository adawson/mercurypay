<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class RecordNo
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class RecordNo extends AbstractEnumeration
{
    const RECORD_NUMBER_REQUESTED = 'RecordNumberRequested';
}