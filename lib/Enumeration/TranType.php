<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class TranType
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class TranType extends AbstractEnumeration
{
    const CREDIT = 'Credit';
}