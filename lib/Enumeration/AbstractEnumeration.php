<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class AbstractEnumeration
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
abstract class AbstractEnumeration
{
    /**
     * Private constructor as all enumeration classes
     * act as static namespaces
     */
    final private function __construct()
    {
    }
}