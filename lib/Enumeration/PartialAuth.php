<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class PartialAuth
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class PartialAuth extends AbstractEnumeration
{
    /**
     * Allow partial authorization
     */
    const ALLOW = 'Allow';
}