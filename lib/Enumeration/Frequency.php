<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class Frequency
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class Frequency extends AbstractEnumeration
{
    const ONE_TIME = 'OneTime';
}