<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class CmdResponseStatus
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class CmdResponseStatus extends AbstractEnumeration
{
    /**
     * APPROVED:
     *
     * credit response
     */
    const AP = 'AP';

    /**
     * APPROVED:
     *
     * Debit and Prepaid response: transaction approved by processor
     */
    const APPROVED = 'APPROVED';

    /**
     * APPROVED:
     *
     * Credit card branded Pre-paid Debit or Gift cards response:
     * - The response is based on the use of <PartialAuth> ALLOW</PartialAuth> in the request;
     * - it may require a split tender and a balance-due prompt;
     * - always confirm <Authorize> amount approved.
     */
    const PARTIAL_AP = 'PARTIAL AP';

    /**
     * APPROVED:
     *
     * Credit response: optional message; account information is updated—this is often the address
     * or expiration date
     */
    const AP_NEW_INFO = 'AP NEW INFO';

    /**
     * APPROVED:
     *
     * Indicates that duplicate logic is enabled and has detected the same card, the same
     * amount, and the same invoice number in a second transaction in the same batch; it is
     * paired with <CmdStatus>Approved</CmdStatus> and assures that the cardholder is
     * charged only once.
     */
    const AP_DUPE = 'AP DUPE';

    /**
     * APPROVED:
     *
     * Indicates that duplicate logic is enabled and has detected the same card, the same
     * amount, and the same invoice number in a second transaction in the same batch; it is
     * paired with <CmdStatus>Approved</CmdStatus> and assures that the cardholder is
     * charged only once.
     */
    const AP_STAR = 'AP*';

    /**
     * APPROVED:
     *
     * Mercury Stand-in Credit, Debit, EBT, and Check response: Transactions approved by
     * Mercury's Stand-in servers during a back-end network outage; it is always accompanied
     * with a MERCXX authorization code.
     */
    const APPROVED_STANDIN = 'APPROVED STANDIN';

    /**
     * APPROVED:
     *
     * Credit, Debit, EBT and Check response: Transactions are being sent to the back-end
     * processing networks but are not being captured or logged with the issuing bank.
     * Typically this only occurs prior to a back-end network outage. It requires the transaction
     * to be resent for authorization.
     */
    const AP_NOT_CAPTURED  = 'AP – NOT CAPTURED';

    /**
     * APPROVED:
     *
     * Credit reversal response: The transaction was acknowledged as reversed by the cardissuing
     * bank when sending a reversal (VoidSale + AcqRefData + ProcessData).
     */
    const REVERSED = 'REVERSED';

    /**
     * DECLINE:
     *
     * Authorization was declined based on issuing bank response. Card brand regulations
     * stipulate that declines should not be re-attempted but should be followed-up with a
     * request for a second form of payment
     */
    const DECLINE = 'DECLINE';

    /**
     * DECLINE:
     *
     * Authorization Declined; authorization network requests picking up the card and
     * reporting. See card brand regulations for details and appropriate action.
     */
    const PIC_UP = 'PIC UP';

    /**
     * DECLINE:
     *
     * Card brand response for CVV2 mismatch. (AMEX uses INVLD CID)
     */
    const DECLINED_CV2_FAIL = 'DECLINED-CV2 FAIL';

    /**
     * DECLINE:
     *
     * Expiration date entered is incorrect, invalid or no longer current.
     */
    const INVALID_EXP_DATE = 'INVALID EXP DATE';

    /**
     * DECLINE:
     *
     * Incorrect PIN entered on Debit or EBT transaction.
     */
    const INVALID_PIN = 'INVALID PIN';

    /**
     * DECLINE:
     *
     * Merchant setting does not allow debit (may also be seen with EBT).
     */
    const UNAUTH_USER = 'UNAUTH USER';

    /**
     * DECLINE:
     *
     * Failed Reversal attempt.
     */
    const NO_TRAN_FOUND = 'NO TRAN FOUND';

    /**
     * DECLINE:
     *
     * Typically triggered by an invalid RefNo used in VoidSales or an Adjust.
     */
    const INV_ITEM_NUMBER = 'INV ITEM NUMBER';
    
    /**
     * DECLINE:
     *
     * Issuer has placed a notice on the card and requests the merchant to call for a Voice
     * Authorization. xxx = " ND," " AE," or " Discover".
     */
    const CALL_CENTER = 'CALL CENTER';
    const CALL_AE = 'CALL AE';
}