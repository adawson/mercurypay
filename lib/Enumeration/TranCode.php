<?php

namespace RevoPos\MercuryPay\Enumeration;

/**
 * Class TranCode
 *
 * @package RevoPos\MercuryPay\Enumeration
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
final class TranCode extends AbstractEnumeration
{
    /**
     * Sale transaction code
     */
    const SALE = 'Sale';

    /**
     * Pre auth transaction code
     */
    const PRE_AUTH = 'PreAuth';

    /**
     * Pre auth capture transaction code
     */
    const PRE_AUTH_CAPTURE = 'PreAuthCapture';

    /**
     * Void sale transaction code
     */
    const VOID_SALE = 'VoidSale';
}