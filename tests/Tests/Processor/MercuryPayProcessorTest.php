<?php

namespace RevoPos\MercuryPay\Tests\Processor;

use RevoPos\MercuryPay\Processor\MercuryPayProcessor;

/**
 * Class MercuryPayProcessorTest
 *
 * @package RevoPos\MercuryPay\Tests\Processor
 * @author Ashley Dawson <ashley@ashleydawson.co.uk>
 */
class MercuryPayProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MercuryPayProcessor
     */
    private $processor;

    /**
     * @var string
     */
    private $testSwipeDeviceOutput;

    /**
     * Set up test case
     */
    protected function setUp()
    {
        $this->processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '019588466313922', 'xyz');

        $this->testSwipeDeviceOutput = '%B4802000050006719^RODRIGUEZ/RALPH ^1611000000000000000000000000000?;4802000050006719=161100000000000?|0600|E973A61929EBD6FBB5D79251B506C307B7C08E4BF138222B83508BF551F9F178C26AE239A7921F253ACAAD2934CD7B97169389896307F263B557E010844DFB25FE6D3589889336C4|F6B2176DB2E9B647A70F610F48D7E4351F6F489163EEDDC55F169F17E1BF5FFFCD688113ACD72EE8||61403000|BB13E44E806B0C4DC29DB68BBA3FA99A4D867427F293A03BD7A3A73D36D415CA66EAC366F11319E1653BBD2BBDFC09FA2750267ABC05402F|B2869DB093014AA|ED8CB74D7F24CE7F|9012110B2869DB000001|9544||1000';
        
        //$this->testSwipeDeviceOutput = '0~IPAD100KB|24~4353047B121C130A|1~21|2~5E97C48238415ACB9C82F4E8E71746127881B8A71FFBB3CC1DCE4B5B83562A871DCC5025C21ECE1BF037C5B069BEA18E|3~ACFBE99088544FDE8393AF9FF2817F5D2E28F99590F7C82C487096465107966E907AA64B45792E4A|4~|5~1F4E65350FD407CACFDC01C9964AC771C88AA7546CC2E553912DA608216A9519675C6FF38FDA2DAD94F0383062AB6A414F39C42E623568D0|6~%B4003000001006781^TEST/MPS^13050000000000000?|7~;4003000001006781=13050000000000000000?|8~|9~00000000|10~000001|11~9500570000008720001D|12~00002200|13~9A006300000009200004|14~8F8A60DFECF190AA|';
    }

    /**
     * Test positive pre auth procedure
     */
    public function testPreAuthSuccess()
    {
        // Low txn value promotes approved response
        $amount = 6.37;

        /** @var $response \RevoPos\MercuryPay\Model\Response\PaymentResponse */
        list ($response, $invoiceNumber) = $this->performPreAuthSwipedTxn($amount, 'Sample Txn');
        
        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);
        
        $this->assertTrue($response->getIsSuccess());
        
        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('PreAuth', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertNull($response->getRStream()->getTranResponse()->getCaptureStatus());

        $this->assertEquals($invoiceNumber, $response->getRStream()->getTranResponse()->getInvoiceNo());

        $this->assertNull($response->getRStream()->getTranResponse()->getRefNo());
        
        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getPurchase());

        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getAuthorize());
        
        $this->assertNull($response->getRStream()->getTranResponse()->getAmount()->getGratuity());

        $this->assertNull($response->getRStream()->getTranResponse()->getRefNo());
    }

    /**
     * Test negative pre auth procedure
     */
    public function testPreAuthFailure()
    {
        // High txn value promotes approved response
        $amount = 28.91;

        /** @var $response \RevoPos\MercuryPay\Model\Response\PaymentResponse */
        list ($response, $invoiceNumber) = $this->performPreAuthSwipedTxn($amount, 'Sample Txn');

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertFalse($response->getIsSuccess());

        $this->assertEquals('Declined', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('PreAuth', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertNull($response->getRStream()->getTranResponse()->getCaptureStatus());

        $this->assertEquals($invoiceNumber, $response->getRStream()->getTranResponse()->getInvoiceNo());

        $this->assertNull($response->getRStream()->getTranResponse()->getRefNo());
        
        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getPurchase());

        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getAuthorize());

        $this->assertNull($response->getRStream()->getTranResponse()->getAmount()->getGratuity());
    }

    /**
     * Test positive pre auth capture (with gratuity adjustment) procedure
     */
    public function testPreAuthCaptureWithGratuitySuccess()
    {
        // Low txn value promotes approved response
        $amount = 5.23;
        $gratuityAmount = 1.23;
        $memo = 'Sample Txn';

        /** @var $response \RevoPos\MercuryPay\Model\Response\PaymentResponse */
        list ($response, $invoiceNumber) = $this->performPreAuthSwipedTxn($amount, $memo);

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertTrue($response->getIsSuccess());

        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('PreAuth', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertNull($response->getRStream()->getTranResponse()->getCaptureStatus());
        
        // Try to capture the payment with a gratuity adjustment
        $response = $this->processor->capturePaymentWithGratuitySwipedPayment(
            $response->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $memo,
            $response->getRStream()->getTranResponse()->getAcqRefData(),
            $amount,
            $gratuityAmount,
            $this->testSwipeDeviceOutput
        );

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertTrue($response->getIsSuccess());

        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());
        
        $this->assertEquals('Captured', $response->getRStream()->getTranResponse()->getCaptureStatus());

        $this->assertEquals('PreAuthCapture', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getPurchase());

        $this->assertEquals($gratuityAmount, $response->getRStream()->getTranResponse()->getAmount()->getGratuity());

        $this->assertEquals($amount + $gratuityAmount, (float) $response->getRStream()->getTranResponse()->getAmount()->getAuthorize());
        
        $this->assertEquals($invoiceNumber, $response->getRStream()->getTranResponse()->getInvoiceNo());

        $this->assertNotNull($response->getRStream()->getTranResponse()->getRefNo());
    }

    /**
     * Test positive pre auth capture (with gratuity adjustment and void) procedure
     */
    public function testPreAuthCaptureWithGratuityAndVoidSuccess()
    {
        // Low txn value promotes approved response
        $amount = 4.37;
        $gratuityAmount = 2.12;
        $memo = 'Sample Txn';

        /** @var $response \RevoPos\MercuryPay\Model\Response\PaymentResponse */
        list ($response, $invoiceNumber) = $this->performPreAuthSwipedTxn($amount, $memo);

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertTrue($response->getIsSuccess());

        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('PreAuth', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertNull($response->getRStream()->getTranResponse()->getCaptureStatus());

        // Try to capture the payment with a gratuity adjustment
        $response = $this->processor->capturePaymentWithGratuitySwipedPayment(
            $response->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $memo,
            $response->getRStream()->getTranResponse()->getAcqRefData(),
            $amount,
            $gratuityAmount,
            $this->testSwipeDeviceOutput
        );

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertTrue($response->getIsSuccess());

        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('Captured', $response->getRStream()->getTranResponse()->getCaptureStatus());

        $this->assertEquals('PreAuthCapture', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getPurchase());

        $this->assertEquals($gratuityAmount, $response->getRStream()->getTranResponse()->getAmount()->getGratuity());

        $this->assertEquals($amount + $gratuityAmount, (float) $response->getRStream()->getTranResponse()->getAmount()->getAuthorize());

        $this->assertEquals($invoiceNumber, $response->getRStream()->getTranResponse()->getInvoiceNo());

        $this->assertNotNull($response->getRStream()->getTranResponse()->getRefNo());
        
        // Try to void the transaction
        $response = $this->processor->processVoidSaleSwipedPayment(
            $response->getRStream()->getTranResponse()->getAuthCode(), 
            $invoiceNumber,
            $response->getRStream()->getTranResponse()->getRefNo(), 
            $memo, 
            $amount, 
            $gratuityAmount,
            $this->testSwipeDeviceOutput
        );

        $this->assertInstanceOf('RevoPos\MercuryPay\Model\Response\PaymentResponse', $response);

        $this->assertTrue($response->getIsSuccess());

        $this->assertEquals('Approved', $response->getRStream()->getCmdResponse()->getCmdStatus());

        $this->assertEquals('Captured', $response->getRStream()->getTranResponse()->getCaptureStatus());

        $this->assertEquals('VoidSale', $response->getRStream()->getTranResponse()->getTranCode());

        $this->assertEquals('VOIDED', $response->getRStream()->getTranResponse()->getAuthCode());

        $this->assertEquals($amount, $response->getRStream()->getTranResponse()->getAmount()->getPurchase());

        $this->assertEquals($gratuityAmount, $response->getRStream()->getTranResponse()->getAmount()->getGratuity());

        $this->assertEquals($amount + $gratuityAmount, (float) $response->getRStream()->getTranResponse()->getAmount()->getAuthorize());
    }

    /**
     * Perform pre auth swiped txn
     *
     * @param float $amount
     * @param string $memo
     * @return array
     */
    private function performPreAuthSwipedTxn($amount, $memo)
    {
        $invoiceNumber = sprintf('%07d', mt_rand(0, 999));

        $response = $this->processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $this->testSwipeDeviceOutput
        );
        
        return array(
            $response,
            $invoiceNumber,
        );
    }
}