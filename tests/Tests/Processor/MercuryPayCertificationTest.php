<?php

namespace RevoPos\MercuryPay\Tests\Processor;

use RevoPos\MercuryPay\Logger\TransactionLogger;
use RevoPos\MercuryPay\Processor\MercuryPayProcessor;

/**
 * Class MercuryPayCertificationTest
 *
 * @package RevoPos\MercuryPay\Tests\Processor
 */
class MercuryPayCertificationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * 1.1
     */
    public function testMerchantAccountFunctionality_1_1()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '003503902913105', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 1.1',
            '1.01',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 1.1', $processor->getTxnLogger());
    }

    /**
     * 1.2
     */
    public function testMerchantAccountFunctionality_1_2()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '023358150511666', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 1.2',
            '1.02',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 1.2', $processor->getTxnLogger());
    }

    /**
     * 1.3
     */
    public function testMerchantAccountFunctionality_1_3()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 1.3',
            '1.03',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 1.3', $processor->getTxnLogger());
    }








    
    
    
    
    
    
    



    /**
     * 2.1
     */
    public function testCardBrandValidation_2_1()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 2.1',
            '2.04',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~4569046A14929BC3014C2BD6945C82C01C315994D84554D33F5AC9B9575BED14E065AF02183D73B8F08B16CF61644508|3~BEF5DB3039D36440283E0CEAB5755E6444FE7D11367B481A8F2C076E4FE1A6091FE981264A4F710C|4~|5~60798FCCE4768BE93944EC3C6774AAD0733796970F1DD2179A69AC913467FAC25F835111CCD35C98C80E01A0CFC637830E821F423963054E|6~%B373953006001001^TEST/MPS^19120000000000000?|7~;373953006001001=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200040|12~00000200|'
        );

        $this->writeTxnToReport('Test 2.1', $processor->getTxnLogger());
    }

    /**
     * 2.2
     */
    public function testCardBrandValidation_2_2()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 2.2',
            '2.05',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~F29D17F457EBE2D2F1D0F989FDAFCCB3A75D2087CC8FEEFA7F8A89D1F85F6F7B69C9BF19F6C20003C96B9789EB08A5F6|3~3CEB5C4F7AD5E6DA7AF213394BD3B299A0344FD54DFC1DC930A8678222B2BC84E60CEF7BB2CB5CE5|4~|5~6A82FF8D021B6E18A47AE6278CD3DA98CA9389ECEA4436E4EB36ACDC33E22BFF50FEA9DC4358792EA070AD91F355DC4DACBC80D265C9B64F|6~%B5439750002000248^TEST/MPS^15120000000000000?|7~;5439750002000248=15120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200043|12~00000200|'
        );

        $this->writeTxnToReport('Test 2.2', $processor->getTxnLogger());
    }

    /**
     * 2.3
     */
    public function testCardBrandValidation_2_3()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 2.3',
            '2.06',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702E2FA0C86C6F0E4DEF7FD99FC04A676174D9DB4C508FC0C22B3ACD992F2931296AB860247BFB473B68D3E9C097614A|3~A2FADC04E7537C468FED9A3338C48D019E459CBB3FE5807E8761F07599849E8F1B590C11C3422B3A|4~|5~BF0F56A93BCCABFA6E718FA9B4D8032C711ECA40DEFCCDF7364D5E353C7D7FD19CB76458665CA8DA954BA2E061C4CB9B80CBB96FD0FB0A4B|6~%B6011900006005677^TEST/MPS^15120000000000000?|7~;6011900006005677=15120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200042|12~00000200|'
        );

        $this->writeTxnToReport('Test 2.3', $processor->getTxnLogger());
    }

    /**
     * 2.4
     */
    public function testCardBrandValidation_2_4()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 2.4',
            '2.07',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 2.4', $processor->getTxnLogger());
    }















    /**
     * 3.1
     */
    public function testDuplication_3_1()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $invoiceNumber = 20;
        $memo = 'MercuryPay Certification Test 3.1';
        $amount = '3.07';
        $gratuityAmount = 0;
        $testSwipeDeviceOutput = '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|';
        
        $response = $processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.1 (PreAuth)', $processor->getTxnLogger());
        
        $processor->capturePaymentWithGratuitySwipedPayment(
            $response->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $memo,
            $response->getRStream()->getTranResponse()->getAcqRefData(),
            $amount,
            $gratuityAmount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.1 (PreAuthCapture)', $processor->getTxnLogger());
    }

    /**
     * 3.2
     */
    public function testDuplication_3_2()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $invoiceNumber = 21;
        $memo = 'MercuryPay Certification Test 3.2';
        $amount = '3.07';
        $gratuityAmount = 0;
        $testSwipeDeviceOutput = '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|';

        $response = $processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.2 (PreAuth)', $processor->getTxnLogger());

        $processor->capturePaymentWithGratuitySwipedPayment(
            $response->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $memo,
            $response->getRStream()->getTranResponse()->getAcqRefData(),
            $amount,
            $gratuityAmount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.2 (PreAuthCapture)', $processor->getTxnLogger());
    }

    /**
     * 3.3 + 3.4
     */
    public function testDuplication_3_3()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $invoiceNumber = 23;
        $memo = 'MercuryPay Certification Test 3.3';
        $amount = '989.30';
        $gratuityAmount = 0;
        $testSwipeDeviceOutput = '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|';

        $response = $processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.3 (PreAuth)', $processor->getTxnLogger());

        if ( ! $response->getRStream()->getTranResponse()) {

            $response = $processor->processPreAuthSwipedPayment(
                $invoiceNumber,
                $memo,
                $amount,
                $testSwipeDeviceOutput
            );

            $this->writeTxnToReport('Test 3.4 (PreAuth) (RESENDING DUE TO TIMEOUT)', $processor->getTxnLogger());
            
        }
    }

    /**
     * 3.5
     */
    public function testDuplication_3_5()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $invoiceNumber = 23;
        $memo = 'MercuryPay Certification Test 3.5';
        $amount = '989.99';
        $gratuityAmount = 0;
        $testSwipeDeviceOutput = '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|';

        $response = $processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 3.5 (PreAuth)', $processor->getTxnLogger());

        if ( ! $response->getRStream()->getTranResponse()) {

            $this->writeArbToReport("*** TIMEOUT ***\n\n");
        }
    }




















    /**
     * 5.6 + 5.7 + 5.8
     */
    public function testSaleSequences_5_6_5_7_5_8()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $invoiceNumber = 30;
        $memo = 'MercuryPay Certification Test 5.6,7,8';
        $amount = '5.04';
        $gratuityAmount = '1.00';
        $testSwipeDeviceOutput = '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|';

        $response1 = $processor->processPreAuthSwipedPayment(
            $invoiceNumber,
            $memo,
            $amount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 5.6 (PreAuth)', $processor->getTxnLogger());

        $response2 = $processor->capturePaymentWithGratuitySwipedPayment(
            $response1->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $memo,
            $response1->getRStream()->getTranResponse()->getAcqRefData(),
            $amount,
            $gratuityAmount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 5.7 (PreAuthCapture + Gratuity)', $processor->getTxnLogger());

        $processor->processVoidSaleSwipedPayment(
            $response2->getRStream()->getTranResponse()->getAuthCode(),
            $invoiceNumber,
            $response2->getRStream()->getTranResponse()->getRefNo(),
            $memo,
            $amount,
            $gratuityAmount,
            $testSwipeDeviceOutput
        );

        $this->writeTxnToReport('Test 5.8 (Void)', $processor->getTxnLogger());
    }
















    /**
     * 9.1
     */
    public function testReferralResponses_9_1()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.1',
            '23.01',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.1', $processor->getTxnLogger());
        
        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.2
     */
    public function testReferralResponses_9_2()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.2',
            '23.04',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702A0691E5DCD106D8429077BFA081DA53BAC3C715343CFC4D3EA88919916D8DE1C41BCA1159C1F864A25F38AF2B7E7C|3~BD34151A12029287899975EA52FFDCDBFAD5F49AF5B10BF241E5AAD0AECF69FA8DF62CBE38327B3E|4~|5~D0F00C034BAA56A73EF07823FC3B88B61BED6D5FD4ED269A98820FBDEEDDE4DEE9CB12AE94AE3AEC0604E6CD52BF654427F98CE0371B172F|6~%B4005550000000480^TEST/MPS^19120000000000000?|7~;4005550000000480=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200041|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.2', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.3
     */
    public function testReferralResponses_9_3()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.3',
            '23.01',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~702E2FA0C86C6F0E4DEF7FD99FC04A676174D9DB4C508FC0C22B3ACD992F2931296AB860247BFB473B68D3E9C097614A|3~A2FADC04E7537C468FED9A3338C48D019E459CBB3FE5807E8761F07599849E8F1B590C11C3422B3A|4~|5~BF0F56A93BCCABFA6E718FA9B4D8032C711ECA40DEFCCDF7364D5E353C7D7FD19CB76458665CA8DA954BA2E061C4CB9B80CBB96FD0FB0A4B|6~%B6011900006005677^TEST/MPS^15120000000000000?|7~;6011900006005677=15120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200042|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.3', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.4
     */
    public function testReferralResponses_9_4()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.4',
            '23.01',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~F29D17F457EBE2D2F1D0F989FDAFCCB3A75D2087CC8FEEFA7F8A89D1F85F6F7B69C9BF19F6C20003C96B9789EB08A5F6|3~3CEB5C4F7AD5E6DA7AF213394BD3B299A0344FD54DFC1DC930A8678222B2BC84E60CEF7BB2CB5CE5|4~|5~6A82FF8D021B6E18A47AE6278CD3DA98CA9389ECEA4436E4EB36ACDC33E22BFF50FEA9DC4358792EA070AD91F355DC4DACBC80D265C9B64F|6~%B5439750002000248^TEST/MPS^15120000000000000?|7~;5439750002000248=15120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200043|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.4', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.5
     */
    public function testReferralResponses_9_5()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.5',
            '23.28',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~F29D17F457EBE2D2F1D0F989FDAFCCB3A75D2087CC8FEEFA7F8A89D1F85F6F7B69C9BF19F6C20003C96B9789EB08A5F6|3~3CEB5C4F7AD5E6DA7AF213394BD3B299A0344FD54DFC1DC930A8678222B2BC84E60CEF7BB2CB5CE5|4~|5~6A82FF8D021B6E18A47AE6278CD3DA98CA9389ECEA4436E4EB36ACDC33E22BFF50FEA9DC4358792EA070AD91F355DC4DACBC80D265C9B64F|6~%B5439750002000248^TEST/MPS^15120000000000000?|7~;5439750002000248=15120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200043|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.5', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.6
     */
    public function testReferralResponses_9_6()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.6',
            '23.01',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~4569046A14929BC3014C2BD6945C82C01C315994D84554D33F5AC9B9575BED14E065AF02183D73B8F08B16CF61644508|3~BEF5DB3039D36440283E0CEAB5755E6444FE7D11367B481A8F2C076E4FE1A6091FE981264A4F710C|4~|5~60798FCCE4768BE93944EC3C6774AAD0733796970F1DD2179A69AC913467FAC25F835111CCD35C98C80E01A0CFC637830E821F423963054E|6~%B373953006001001^TEST/MPS^19120000000000000?|7~;373953006001001=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200040|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.6', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }

    /**
     * 9.7
     */
    public function testReferralResponses_9_7()
    {
        $processor = new MercuryPayProcessor('https://w1.mercurydev.net/ws/ws.asmx?WSDL', '118725340908147', 'xyz');

        $response = $processor->processPreAuthSwipedPayment(
            $this->getRandomInvoiceNumber(),
            'MercuryPay Certification Test 9.7',
            '23.18',
            '0~IPAD100KB|24~98C898A9110E1D0D|1~11|2~4569046A14929BC3014C2BD6945C82C01C315994D84554D33F5AC9B9575BED14E065AF02183D73B8F08B16CF61644508|3~BEF5DB3039D36440283E0CEAB5755E6444FE7D11367B481A8F2C076E4FE1A6091FE981264A4F710C|4~|5~60798FCCE4768BE93944EC3C6774AAD0733796970F1DD2179A69AC913467FAC25F835111CCD35C98C80E01A0CFC637830E821F423963054E|6~%B373953006001001^TEST/MPS^19120000000000000?|7~;373953006001001=19120000000000000000?|8~|9~00000000|10~000001|11~950003000000E5200040|12~00000200|'
        );

        $this->writeTxnToReport('Test 9.7', $processor->getTxnLogger());

        $this->writeArbToReport("Computed Response: " . $response->getMessage() . "\n\n");
    }
    
    

    
    
    
    
    
    
    
    
    private function writeTxnToReport($header, TransactionLogger $logger)
    {
        $h = fopen(__DIR__ . '/../../Resources/reports/CertificationReport.txt', 'a');
        
        $entry  = '## ' . $header . "\n";
        $entry .= "------------------------------------------------------------------------------------------\n\n";
        $entry .= "URL: " . $logger->getUrl() . "\n";
        $entry .= "MID: " . $logger->getMerchantId() . "\n";
        $entry .= "Password: " . $logger->getPassword() . "\n";
        $entry .= "\n--REQUEST------------------------------------\n";
        $entry .= (string) $logger->getRequest();
        $entry .= "\n--RESPONSE-----------------------------------\n";
        $entry .= (string) $logger->getResponse();
        $entry .= "\n\n";
        
        fwrite($h, $entry);
        fclose($h);
    }

    private function writeArbToReport($message)
    {
//        $h = fopen(__DIR__ . '/../../Resources/reports/CertificationReport.txt', 'a');
//        fwrite($h, $message);
//        fclose($h);
    }
    
    private function getRandomInvoiceNumber()
    {
        return '000' . mt_rand(10, 9999);
    }
}